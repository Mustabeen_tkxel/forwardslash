@extends('layouts.master')

@section('title', 'Dashboard')

@section('content')
    <h2>Report</h2>
    <table>
        <tr>
            <th>Quote</th>
            <th>Fetch Count</th>
            <th>Fetch Speed</th>
            <th>Fetch Day</th>
            <th>Fetch Time</th>
        </tr>
        @if(!empty($quotes))
            @foreach($quotes as $qoute)
                <tr>
                    <td>{{$qoute['quote']}}</td>
                    <td>{{$qoute['fetch_count']}}</td>
                    <td>{{$qoute['details']['fetch_speed']}}</td>
                    <td>{{$qoute['details']['fetch_day']}}</td>
                    <td>{{$qoute['created_at']}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">There are no data.</td>
            </tr>
        @endif
    </table>
    {{ $quotes->links() }}
@endsection
