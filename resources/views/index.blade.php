@extends('layouts.master')

@section('title', 'Dashboard')

@section('content')
        <h2>Quotes</h2>
        <a class="btn btn-primary float-right mb-1" href="{{route('quotes')}}" role="button">Refresh</a>
        <table>
            <tr>
                <th>Quote</th>
            </tr>
            @if(!empty($quotes))
                @foreach($quotes as $qoute)
                    <tr>
                        <td>{{$qoute['quote']}}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10">There are no data.</td>
                </tr>
            @endif
        </table>
@endsection
