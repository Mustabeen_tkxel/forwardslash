<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
    <h1 class="h2">OOPS!! </h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="{{url()->previous()}}"> <button class="btn btn-sm btn-outline-secondary">Go Back</button></a>
        </div>
    </div>
</div>
<div class="text-center font-weight-bold">
    Some Thing Went Wrong. Please Contact System Administrator to fix this.
</div>
