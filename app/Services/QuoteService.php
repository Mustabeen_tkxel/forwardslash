<?php

namespace App\Services;

use App\Repositories\QuoteDetailRepository;
use App\Repositories\QuoteRepository;
use Illuminate\Support\Facades\Log;

class QuoteService
{
    /**
     * @var null
     */
    private $quoteRepository = null;

    /**
     * @var null
     */
    private $quoteDetailRepository = null;

    /**
     * QuoteService constructor.
     * @param QuoteRepository $quoteRepository
     * @param QuoteDetailRepository $quoteDetailRepository
     */
    public function __construct(
        QuoteRepository $quoteRepository,
        QuoteDetailRepository $quoteDetailRepository
    )
    {
        $this->quoteRepository = $quoteRepository;
        $this->quoteDetailRepository = $quoteDetailRepository;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function fetchAndSaveQuote()
    {
        try {
            $url = env('API_URL', 'https://api.kanye.rest/?format=text');
            $headers = [
                'Content-Type: application/json',
            ];
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);

            $time_start = microtime(true);
            $result = curl_exec($ch);
            $time_end = microtime(true);
            curl_close($ch);

            $execution_time = ($time_end - $time_start);
            $quote = $this->quoteRepository->saveOrUpdate($result);
            $formattedArray = $this->formatArray($quote, $execution_time);
            $this->quoteDetailRepository->save($formattedArray);
        } catch (\Exception $e) {
            Log::error("Service: Quote fetchAndSaveQuote error:" . $e->getMessage());
            return view('errors.error');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fetchQuotes()
    {
        try {
            $limit = env('QUOTE_LIMIT', 5);
            $quotes = $this->quoteRepository->fetchQuotes($limit);
            return $quotes;
        } catch (\Exception $e) {
            Log::error("Service: Quote fetchQuotesDetails error:" . $e->getMessage());
            return view('errors.error');
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listing()
    {
        try {
            $limit = env('QUOTE_LIMIT', 5);
            $quotes = $this->quoteRepository->listing($limit);
            foreach ($quotes as $quote) {
                $quote['details'] = $this->quoteDetailRepository->fetchDetailsByQuoteId($quote->id);
            }

            return $quotes;
        } catch (\Exception $e) {
            Log::error("Service: Quote fetchQuotesDetails error:" . $e->getMessage());
            return view('errors.error');
        }
    }


    /**
     * @param $quote
     * @param $execution_time
     * @return array
     * @throws \Exception
     */
    private function formatArray($quote, $execution_time)
    {
        try {
            $day = date('D', strtotime($quote['updated_at']));
            return [
                'quote_id' => $quote['id'],
                'fetch_speed' => $execution_time,
                'fetch_day' => $day,
            ];
        } catch (\Exception $e) {
            Log::error("Service: Quote formatArray error:" . $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }
}
