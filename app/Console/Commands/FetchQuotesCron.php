<?php

namespace App\Console\Commands;

use App\Services\QuoteService;
use Illuminate\Console\Command;

class FetchQuotesCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:quotes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Quotes and save it in database';

    protected $quoteService = null;


    /**
     * Create a new command instance.
     *
     * @param QuoteService $quoteService
     */
    public function __construct(
        QuoteService $quoteService
    )
    {
        parent::__construct();
        $this->quoteService = $quoteService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->comment("Fetching Quote from api and saving it our Database Starting.");
        $this->quoteService->fetchAndSaveQuote();
        $this->comment("Fetching Quote from api and saving it our Database Ending.");
    }
}
