<?php
namespace App\Repositories;

use DB;
use App\Models\QuoteDetail;
use Illuminate\Support\Facades\Log;

class QuoteDetailRepository
{

    /**
     * @var null
     */
    protected $model = null;

    /**
     * QuoteDetailRepository constructor.
     * @param QuoteDetail $quoteDetail
     */
    public function __construct(QuoteDetail $quoteDetail)
    {
        $this->model = $quoteDetail;
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function save($data)
    {
        try {
            $model = new $this->model();
            $model->fill($data);
            $model->save();

            return $model;
        }  catch (\Exception $e) {
            Log::error("Repository: QuoteDetail save error:" . $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }



    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function fetchDetailsByQuoteId($id)
    {
        try {
            return $this->model
                ->select('id', 'quote_id', 'fetch_day', DB::raw('AVG(fetch_speed) as fetch_speed'))
                ->where('quote_id', $id)
                ->groupBy('id', 'quote_id', 'fetch_day')
                ->first();

        }  catch (\Exception $e) {
            Log::error("Repository: QuoteDetail fetchDetailsByQuoteId error:" . $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

}
