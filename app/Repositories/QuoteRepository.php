<?php
namespace App\Repositories;

use App\Models\Quote;
use Illuminate\Support\Facades\Log;

class QuoteRepository
{

    /**
     * @var null
     */
    protected $model = null;

    /**
     * QuoteRepository constructor.
     * @param Quote $quote
     */
    public function __construct(Quote $quote)
    {
        $this->model = $quote;
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Exception
     */
    public function fetchList(array $attributes = [])
    {
        try {
            if(!empty($attributes)) {
                return $this->model->select($attributes)->orderBy('quote', 'ASC')->get();
            } else {
                return $this->model->orderBy('quote', 'ASC')->get();
            }
        } catch (\Exception $e) {
            Log::alert("Repository: Quote fetchList error:" . $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }


    /**
     * @param $attribute
     * @param $value
     * @return Quote|null
     * @throws \Exception
     */
    public function fetchByAttribute($attribute, $value)
    {
        try {
            if (in_array($attribute, $this->model->getListableColumns())) {
                return $this->model->where($attribute, $value)->first();
            }
            return null;
        }  catch (\Exception $e) {
            Log::error("Repository: Quote fetchById error:" . $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param $quote
     * @return mixed
     * @throws \Exception
     */
    public function saveOrUpdate($quote)
    {
        try {
            $model = $this->fetchByAttribute('quote', $quote);
            if (empty($model)) {
                $model = new Quote();
            }
            $model->quote = $quote;
            $model->fetch_count = $model->fetch_count + 1;
            $model->save();

            return $model;
        }  catch (\Exception $e) {
            Log::error("Repository: Quote saveOrUpdate error:" . $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param $limit
     * @return mixed
     * @throws \Exception
     */
    public function fetchQuotes($limit)
    {
        try {
            return $this->model->inRandomOrder()->limit($limit)->get();
        }  catch (\Exception $e) {
            Log::error("Repository: Quote listing error:" . $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param $limit
     * @return mixed
     * @throws \Exception
     */
    public function listing($limit)
    {
        try {
            return $this->model->simplepaginate($limit);
        }  catch (\Exception $e) {
            Log::error("Repository: Quote listing error:" . $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

}
