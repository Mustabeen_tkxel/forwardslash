<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Quote extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quote',
        'fetch_count',
    ];

    /**
     * The attributes that are going to be displayed in List View.
     *
     * @var array
     */
    protected $listableColumns = [
        'id',
        'quote',
        'fetch_count'
    ];

    /**
     * @return array
     * @throws \Exception
     */
    public function getListableColumns()
    {
        try {
            return $this->listableColumns;
        } catch (\Throwable $e) {
            Log::error('Model: Quotes getListableColumns error: '. $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }


    /**
     * Get the Quote linked the Quote_Detail.
     * @throws \Exception
     */
    public function details()
    {
        try {
            return $this->hasMany('app\Models\QuoteDetail');
        } catch (\Throwable $e) {
            Log::error('Model: Quote details relation error: '. $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

}
