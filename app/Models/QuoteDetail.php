<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class QuoteDetail extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quote_id',
        'fetch_speed',
        'fetch_day'
    ];

    /**
     * The attributes that are going to be displayed in List View.
     *
     * @var array
     */
    protected $listableColumns = [
        'id',
        'quote_id',
        'fetch_speed',
        'fetch_day'
    ];

    /**
     * @return array
     * @throws \Exception
     */
    public function getListableColumns()
    {
        try {
            return $this->listableColumns;
        } catch (\Throwable $e) {
            Log::error('Model: Quote Details getListableColumns error: '. $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }


    /**
     * Get the Quote linked the Quote_Detail.
     * @throws \Exception
     */
    public function quote()
    {
        try {
            return $this->belongsTo('app\Models\Quote');
        } catch (\Throwable $e) {
            Log::error('Model: Quote quote_details relation error: '. $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

}
