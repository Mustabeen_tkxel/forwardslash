<?php

namespace App\Http\Controllers;


use App\Services\QuoteService;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class QuoteController extends Controller
{

    /**
     * @var QuoteService|null
     */
    private $quoteService = null;

    /**
     * HomeController constructor.
     * @param QuoteService $quoteService
     */
    public function __construct(
        QuoteService $quoteService
    )
    {
        $this->quoteService = $quoteService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        try {
            $result = $this->quoteService->fetchQuotes();
            return view('index', [
                'quotes' => $result
            ]);
        } catch (\Exception $e) {
            Log::error("Controller: Quote index error:" . $e->getMessage());
            return view('errors.error');
        }
    }
    

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function report()
    {
        try {
            $result = $this->quoteService->listing();
            return view('report', [
                'quotes' => $result
            ]);
        } catch (\Exception $e) {
            Log::error("Controller: Quote index error:" . $e->getMessage());
            return view('errors.error');
        }
    }
}
