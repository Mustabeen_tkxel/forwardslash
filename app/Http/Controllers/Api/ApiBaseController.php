<?php
/**
 * ApiBaseController is the parent controller for all api controllers
 * @author Mustabeen Iqbal
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

Class ApiBaseController extends Controller
{
    /**
     * @param $status
     * @param $message
     * @param $payLoad
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnResponse($status, $message, $payLoad)
    {
        $response = ['status' => $status, 'message' => $message, 'payload' =>$payLoad];
        return response()->json($response);
    }

    public function testCallback(Request $request){
        $params = $request->all();
        return response()->json(['status' => 404, 'data' => $params]);
    }
}
