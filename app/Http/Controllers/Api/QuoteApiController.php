<?php

namespace App\Http\Controllers\Api;


use App\Services\QuoteService;
use Illuminate\Support\Facades\Log;

class QuoteApiController extends ApiBaseController
{

    /**
     * @var QuoteService|null
     */
    private $quoteService = null;

    /**
     * HomeController constructor.
     * @param QuoteService $quoteService
     */
    public function __construct(
        QuoteService $quoteService
    )
    {
        $this->quoteService = $quoteService;
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function listing()
    {
        try {
            $result = $this->quoteService->fetchQuotes();
            return response()->json($result);

        } catch (\Exception $e) {
            Log::error("Controller: QuoteApi listing error:" . $e->getMessage());
            return response()->json([
                'statusCode' => 201,
                'msg' => $e->getMessage()
            ]);
        }
    }
}
