<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response;

class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->api_token != env('API_KEY')) {
            return response()->json('Unauthorized', Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }
}
