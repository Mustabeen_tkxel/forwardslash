<?php

namespace Tests\Unit;

use Illuminate\Http\Response;
use Tests\TestCase;

class QuoteTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * Unit test to figure out json structure of exposed api
     */
    public function testForExposedApi()
    {
        $this->json('get', '/api/fetchQuotes/p2lbgWkFrykA4QyUmpHihzmc5BNzIABq')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    '*' => [
                        'id',
                        'name',
                        'fetch_count',
                        'created_at',
                        'updated_at',
                    ]
                ]
            );
    }


    /**
     * Unit test to to check api passes the valid key
     */
    public function testForExposedApiKeyValidation()
    {
        $response = $this->get('/api/fetchQuotes/'.env('API_KEY'));
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

    }

    /**
     * Unit test to to check api failes the invalid key
     */
    public function testForExposedApiKeyInValidation()
    {
        $response = $this->get('/api/fetchQuotes/'.env('DB_CONNECTION'));
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());

    }



}
