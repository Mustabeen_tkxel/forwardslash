
## About ForwardSlash

This Project is a web application That includes connecting to an API, basic MVC, exposing an API, persisting data, and finally tests.
The API to connect to is https://kanye.rest/ .

## How to Set up

## Clone the Project

 clone project using 

- **git clone [https://Mustabeen_tkxel@bitbucket.org/Mustabeen_tkxel/forwardslash.git](https://Mustabeen_tkxel@bitbucket.org/Mustabeen_tkxel/forwardslash.git)**

## Installation

- move inside the folder by running "cd forwardslash"
- run "composer install" (Make sure you have composer installed in your system)

## Set up your .env

- create a file .env by running "nano .env" and paste the below mentioned code in it 

        APP_ENV=local
        APP_KEY=base64:Kfi6AVS45ju7EpVdS1nAntjQfk+J77j65ZhtKj770MU=
        APP_DEBUG=true
        APP_URL=http://localhost
        
        API_KEY=p2lbgWkFrykA4QyUmpHihzmc5BNzIABq
        
        LOG_CHANNEL=stack
        
        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=forward_slash
        DB_USERNAME=root
        DB_PASSWORD=
        
        BROADCAST_DRIVER=log
        CACHE_DRIVER=file
        QUEUE_CONNECTION=sync
        SESSION_DRIVER=file
        SESSION_LIFETIME=120
        
        REDIS_HOST=127.0.0.1
        REDIS_PASSWORD=null
        REDIS_PORT=6379
        
        MAIL_DRIVER=smtp
        MAIL_HOST=smtp.mailtrap.io
        MAIL_PORT=2525
        MAIL_USERNAME=null
        MAIL_PASSWORD=null
        MAIL_ENCRYPTION=null
        MAIL_FROM_ADDRESS=null
        MAIL_FROM_NAME="${APP_NAME}"
        
        AWS_ACCESS_KEY_ID=
        AWS_SECRET_ACCESS_KEY=
        AWS_DEFAULT_REGION=us-east-1
        AWS_BUCKET=
        
        PUSHER_APP_ID=
        PUSHER_APP_KEY=
        PUSHER_APP_SECRET=
        PUSHER_APP_CLUSTER=mt1
        
        MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
        MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

- create your database and change its setting in .env file

## Migration

- run "php artisan migrate"  to create table in your database

## Data Population

- run "php artisan fetch:quotes" several times to populate date from external api to our system's database
